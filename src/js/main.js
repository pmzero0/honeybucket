// let jsonData: '';
// // localStorageチェック
// if (window.localStorage) {
//     // JsonDataがキーとしてあるかチェック
//     if (window.localStorage.getItem('jsonData')) {
//         const json = window.localStorage.getItem('jsonData');
//         // 文字列をオブジェクトに変換する
//         jsonData = JSON.parse(json);
//     }
// }
// // localStorageに、キー名とオブジェクトをセット
// function setLocalStorage(key: string, value: '') {
//     const json = JSON.stringify(value);
//     window.localStorage.setItem(key, json);// オブジェクトをlocalStorageに保存
// }
// status毎にフィルターをかける
var filters = {
    all: function (todos) {
        return todos;
    },
    // 現在進行系のtask
    active: function (todos) {
        return todos.filter(function (todo) {
            return !todo.finished;
        });
    },
    // 済task
    finished: function (todos) {
        return todos.filter(function (todo) {
            return todo.finished;
        });
    }
};
// vueでの処理
var main = new Vue({
    // 入力欄、空のオブジェクトをセットする
    newTodo: '',
    // task一覧
    todos: { content: '', finished: false },
    // 対象のDOM要素
    el: '#todoApp',
    // bindingするdataを設定
    data: {
        // タイトル
        title: 'ToDoList for Vue.js',
        // desc
        desc: 'Input your tasks!',
        // style
        fontSize: '16px',
        fontColor: '#3b5047',
        // 表示する
        visibility: 'all',
        // 登録済task
        todos: [
            { content: '病院へ行く', finished: false },
            { content: '再配達の連絡する', finished: false },
            { content: '家賃払う', finished: false },
        ],
    },
    computed: {
        // taskをフィルタリングする
        filteredTodos: function () {
            return filters[this.visibility](this.todos);
        },
    },
    // 実行する処理
    methods: {
        // taskを追加する処理
        addTodo: function () {
            // keyup.enterで入力させる
            this.todos.push({
                content: this.newTodo,
                finished: false,
            });
            this.newTodo = '';
        },
        /**
         * taskを削除する
         */
        onDelete: function (index) {
            this.todos.splice(index, 1);
        },
    },
});
/**
 *  routing:指定されたURLの処理
 *  /#/all からallを持ってきて、main.visibilityに代入
 */
function onHashChange() {
    var visibility = window.location.hash.replace(/#\/?/, '');
    if (filters[visibility]) {
        main.$data.visibility = visibility;
    }
    else {
        window.location.hash = '';
        main.$data.visibility = 'all';
    }
}
;
window.addEventListener('hashchange', onHashChange);
onHashChange();
