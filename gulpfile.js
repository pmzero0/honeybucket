var gulp = require("gulp");
var ts = require("gulp-typescript");
 
gulp.task("default", function () {
  // TypeScriptのファイルが配置されている場所
  gulp.src("src/ts/*")
      .pipe(ts({
        // ECMAScript5向けに書き出す
        target: "ES5",
        // コメントの削除
        removeComments: false
      }))
      // jsプロパティを参照
      .js
      // 書き出し先の指定
      .pipe(gulp.dest("src/js"));
});